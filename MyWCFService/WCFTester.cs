﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WCFTester" in both code and config file together.
    public class WCFTester : IWCFTester
    {
        public string HelloWorld()
        {
            return "Hello World";
        }

        public string SayHello(string name)
        {
            return "Hello "+ name;
        }
    }
}
