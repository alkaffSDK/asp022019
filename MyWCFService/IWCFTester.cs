﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWCFTester" in both code and config file together.
    [ServiceContract]
    public interface IWCFTester
    {
        [OperationContract]
        string HelloWorld();

        [OperationContract]
        string SayHello(string name);
    }
}
