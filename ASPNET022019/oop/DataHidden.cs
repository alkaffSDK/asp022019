﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{

    class Point
    {
        public static int counter = 10;
        public   int x, y, z;

       public void NonStaticmethod()
        {
            x = 10;
            counter = 0;
        } 

        public static void StaticMethod()
        {
            // x = 10;          // instance (non static  variables are not accessable from static methods
            counter = 0;
        }
    }
    class DataHidden
    {
        // Data abstraction 

        static void Main(string[] args)
        {
            // call static members using class name 
            Point.StaticMethod();
            Point.counter = 2;

            //int x, y, z;
            Point p0 = new Point();
            p0.x = 10;
            p0.y = 20;
            p0.z = 30;

            //int x1, y1, z1;

            // call non static members using variable name
            p0.NonStaticmethod();
            p0.x = 2;


            Point p1 = new Point() ;
            p1.x = 1;
            p1.y = 2;
            p1.z = 3;

            Console.WriteLine("P0:{0},{1},{2}",p0.x,p0.y,p0.z);
            Console.WriteLine("P1:{0},{1},{2}", p1.x, p1.y, p1.z);

            // swap variables 
            Point t = p0;
            p0 = p1;
            p1 = t;


            Console.WriteLine("P0:{0},{1},{2}", p0.x, p0.y, p0.z);
            Console.WriteLine("P1:{0},{1},{2}", p1.x, p1.y, p1.z);


            Point.counter = 20;

            Console.ReadKey();

        }
    }
}
