﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{

    class Color
    {
        private string Name;

        public void SetName(string name)
        {
            this.Name = name;
        }
    }

    class SubColor: Color
    {
       

        public void SetName(string Name)
        {
            // will call the setName of the Color class (Parent object)
            base.SetName(Name);
        }
    }
    class ThisAndBase
    {


        static void Main(string[] args)
        {
            
        }
    }
}
