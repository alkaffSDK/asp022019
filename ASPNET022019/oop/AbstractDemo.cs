﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    // abstract method : is a method without a body, but it can be exist only in a abstract class 
    // abstract class : is the class that may containt an abstract methods 

    // non virtual or abstract	 methods can NOT be overridden
    // virtual methods  can be overridden
    // abstract methods must be overriden

    abstract class Shape1
    {
        // abstract class can have data members or non abstract methods 
        int a; 
        public Shape1()
        {
            Console.WriteLine("Shape1()");
        }
        /// <summary>
        /// 
        /// </summary>
        public void Method()
        {
        }
        public abstract void AbstractMethod();
        public  abstract double Area();
    }

    class Rectangle : Shape1
    {
        /// <summary>
        /// Width of the rectangle
        /// </summary>
        public int Width  { get; set; }
        public int Height { get; set; }
        /// <summary>
        /// This is the constructor of the rectangle class
        /// </summary>
        /// <param name="w">The width value</param>
        /// <param name="h">The hight value</param>
        public Rectangle(int w, int h)
        {
            Width  = w;
            Height = h;
        }
        public override void AbstractMethod()
        {
            Console.WriteLine("AbstractMethod()");
        }
        public override double Area()
        {
            return Width * Height;
        }
    }
     class Squar : Rectangle
    {
        public Squar(int s):base(s,s)
        {}
    }
    class Triangle : Shape1
    {
        public Triangle(int b, int h)
        {
            Base = b;
            Height = h; 
        }
        public int Base { get; set; }
        public int Height { get; set; }
        public override void AbstractMethod()
        {
            Console.WriteLine("");
        }
        public override double Area()
        {
            return 0.5 * Base * Height;
        }
    }
    class AbstractDemo
    {
        static void Main(string[] args)
        {
            // Can't create an instance of an abstract class direclty using new operator
            // becuase you can execute the abstract methods 
            //Shape1 shape = new Shape1();
            //shape.AbstractMethod();
            Rectangle r = new Rectangle(15,5);
            r.AbstractMethod();

            Shape1[] shaps = new Shape1[3];
            shaps[0] = new Rectangle(10,5);
            new Rectangle(15, 2);
            shaps[1] = new Squar(4);
            shaps[2] = new Triangle(5,8);
            "a".Substring(0, 2);
            for (int i = 0; i < shaps.Length; i++)
                shaps[i].Area();

            Console.ReadKey();
        }
    }
}
