﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    class CollectionsDemo
    {
        static void Main(string[] args)
        {
            ArrayList al = new ArrayList();
            Console.WriteLine("Count :{0} , Capacity:{1}",al.Count,al.Capacity);
            al.Add(1);
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Add(2);
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Add(8);
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Add(3);
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Add(7);
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Insert(2, 5);
            al.TrimToSize();
           Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Reverse();
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));
            al.Sort();
            Console.WriteLine("Count :{0} , Capacity:{1} : {2}", al.Count, al.Capacity, PrintCollection(al));


            Hashtable ht = new Hashtable();
            ht.Add("a", "Ahmed");
            ht.Add("A", "Ali");
            ht.Add("b", "Mohammed");

            Console.WriteLine("Only keys");
            foreach (var k in ht.Keys)
                Console.WriteLine("{0,10}", k);

            Console.WriteLine("Only Values");
            foreach (var v in ht.Values)
                Console.WriteLine("{0,10}",v);
        
            Console.WriteLine("Keys and  Values");
            foreach (var k in ht.Keys)
                Console.WriteLine("{0,10} : {1} - {2}", k, ht[k],k.GetHashCode());


            SortedList sl = new SortedList();
            sl.Add("a", "Ahmed");
            sl.Add("A", "Ali");
            sl.Add("b", "Mohammed");
            foreach (var k in sl.Keys)
                Console.WriteLine("{0,10} : {1}", k, sl[k]);


            BitArray ba = new BitArray(32);
            ba.Set(0, true);
            ba.SetAll(true);

            //010001000011110001000

            Console.WriteLine("Stack");
            Stack stack = new Stack();

            stack.Push(1);
            stack.Push(5);
            stack.Push(6);
            stack.Push(9);

            foreach(var v in stack)
            {
                Console.WriteLine(v);
            }

            var poped = stack.Pop();
            Console.WriteLine("After popoing :"+poped);
            foreach (var v in stack)
            {
                Console.WriteLine(v);
            }

            var peeking = stack.Peek();
            Console.WriteLine("After Peek :" + peeking);
            foreach (var v in stack)
            {
                Console.WriteLine(v);
            }
            Queue queue = new Queue();


            Console.WriteLine("Queue");

            queue.Enqueue(1);
            queue.Enqueue(5);
            queue.Enqueue(6);
            queue.Enqueue(9);

            foreach (var v in queue)
            {
                Console.Write(v+ "-->");
            }

            var dec = queue.Dequeue();
            Console.WriteLine("\nAfter Dequeue :" + dec);
            foreach (var v in queue)
            {
                Console.Write(v + "-->");
            }

             peeking = queue.Peek();
            Console.WriteLine("\nAfter Peek :" + peeking);
            foreach (var v in queue)
            {
                Console.Write(v + "-->");
            }

            Console.WriteLine();
            SortedSet<int> sortset = new SortedSet<int>();
            sortset.Add(1);
            sortset.Add(8);
            sortset.Add(5);
            sortset.Add(5);
            
            foreach (var v in sortset)
            {
                Console.Write(v+ ", ");
            }
            Console.WriteLine();

            Console.ReadKey();




        }


        public static string PrintCollection(IList list)
        {
            if (list == null)
                return null;

            if (list.Count == 0)
                return "{}";

            StringBuilder sb = new StringBuilder("{");

            for(int i= 0; i< list.Count; i++)
            {

                sb.Append(list[i]);
                if (i == list.Count - 1)
                    sb.Append("}");
                else
                    sb.Append(", ");
            }

            return sb.ToString();
        }
    }
}
