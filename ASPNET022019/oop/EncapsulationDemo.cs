﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{

    class A
    {
       public int x;
       protected int y;
       private int z;
    }

    class Person
    {
        public  string Name;
        private int Age;

        private string Password;

        public Person ()
        {

        }
        public Person(String n, int a)
        {
            SetAge(a);
            Name = n;
        }
        public override string ToString()
        {
            return String.Format("Person [{0}, {1}]",Name,Age);
        }

        public bool ChangePassword(string pass)
        {
            
            if(!string.IsNullOrEmpty(pass) && 
                ! string.IsNullOrWhiteSpace(pass) && pass.Length >= 6)
            {
                Password = pass;
                return true;
            }
            return false;

        }

        public bool CheckPassword(string pass)
        {
            return pass.Equals(Password);
        }
        public void SetAge(int a)
        {
            if (a > 0 && a <= 150)
                Age = a;
            else
                Age = 0;
        }
        public int GetAge() { return Age; }
    }
    class EncapsulationDemo
    {
       
        static void Main(string[] args)
        {

            Person p = new Person();
            p.Name = "Ahmed";
            p.SetAge(36);
            p.ChangePassword("123456");

            Console.WriteLine(p.CheckPassword("123456"));       // true
            Console.WriteLine(p.CheckPassword("12345"));        // false
            A a = new A();
            a.x = 10;
            //a.y = 2;
            //a.z = 5;


            Person p1 = new Person();

            Console.WriteLine("The person name:{0}, age:{1}",p1.Name, p1.GetAge());
            p1.Name = "Ahmed";
            //p1.Age = -36;

            p1.SetAge(360);
            Console.WriteLine("The person name:{0}, age:{1}", p1.Name, p1.GetAge());

            //p1.Age = -36;
            Console.WriteLine("The person name:{0}, age:{1}", p1.Name, p1.GetAge());

            Console.ReadKey();
        }
    }
}
