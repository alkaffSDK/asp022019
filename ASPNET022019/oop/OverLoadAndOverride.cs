﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    class Parent
    {
        public virtual void Method()
        {
            Console.WriteLine("Parent:Method()");
        }
        public void AnohterMethod()
        {
            Console.WriteLine("Parent:AnohterMethod()");
        }
    }

    class Child : Parent
    {
        public override void Method()
        {
            Console.WriteLine("Child:Method()");
        }

        // Can't override non virtual or abstract or override methods 
        //public override void AnohterMethod()
        //{
        //    Console.WriteLine("Child:Method()");
        //}

    }
    class OverLoadAndOverride
    {

        // Overloading : Defining more than one method using the same name but diffrent parameter list
        static public void add()
        { }
        static public void add(int a)
        { }
        static public void add(int a, int b)
        { }
        static public void add(double d)
        { }
        //public int add()
        //{
        //    return 0;
        //}
        // Overrideing : Re-implemnting an inherited method using the same method name and parameter list
        // In C#: you can only overide the virtual, abstract or override methods 



        static void Main(string[] args)
        {
            add();
            add(10);
            add(15, 17);
            add(15.65);

        }
    }
}
