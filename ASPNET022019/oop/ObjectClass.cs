﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{
    class Shape
    {
        private int[] arr;

        public void SetArray(int[] a)
        {
            arr = a;
        }

        public int[] GetArray() { return arr; }

        public string PrintArray()
        {
            StringBuilder builder = new StringBuilder("{");
            for(int i=0;i<arr.Length;i++)
            {
                builder.Append(arr[i]);
                if (i < arr.Length - 1)
                    builder.Append(", ");
                else
                    builder.Append("}");
            }
            return builder.ToString();
        }
    }

    class Point1
    {
        public int x, y, z, m;

        public Point1()
        {
        }
        public Point1(int a, int b, int c)
        {
            SetX(a);
            SetY(b);
            SetZ(c);
        }

        public void SetX(int value) { if (x > 0 && x < 1000) x = value;}
        public void SetY(int value) { y = value; }
        public void SetZ(int value) { z = value; }

        // Override : is the process of re-implement the inherited method
        public override bool Equals(object a)     // obj = p1 
        {
            Point1 t =  (Point1) a;       // t = p1 ;
            return x == t.x && y == t.y && z == t.z;
        }

        public override string ToString()
        {
            return string.Format("Point : [{0},{1},{2},{3}]", x, y, z,m); 
        }


    }
    class ObjectClass
    {
        static void Main(string[] args)
        {
            Shape s;                     // s is a variable of type Shape 
            Shape s1 = new Shape();     // s1 is a variable of type Shape that
                                        // refrece to an object of type Shape

            object o;                  // o is a variable of type object 

            object o1 = new object(); // o1 is a variable of type object class that
                                      // refrece to an object of type object class
            int a =1 ;

            Point1 p = new Point1();
            p.x = 1;
            p.y = 2;
            p.z = 3;

            //Point1 p1 = p;
            Point1 p1 = new Point1(1,2,3);
            //p1.x = 1;
            //p1.y = 20;
            //p1.z = 3;
            //p1.z = 20;

           
            Object obj = p1; 
            Point1 t = (Point1) obj;


            Console.WriteLine("Point P : [{0},{1},{2}]", p.x,p.y,p.z);
            Console.WriteLine("Point P1: [{0},{1},{2}]", p1.x, p1.y, p1.z);

           

            Console.WriteLine("---------Direct print----------");
            Console.WriteLine(p);   // this will call p.ToString()
            Console.WriteLine(p1);
            Console.WriteLine(obj);

            Console.WriteLine("---------ToString ----------");
            Console.WriteLine(p.ToString());
            Console.WriteLine(p1.ToString());
            Console.WriteLine(obj.ToString());

            Console.WriteLine(p == p1);             // false 

            Console.WriteLine(p.Equals(p1));        // true

            int m = 50;

            Print(10);
            Print(m);

            Point1 p5 = new Point1(5,5,5);

            Console.WriteLine(p5);
            p5.SetY(10);

            Console.WriteLine(p5);

            Shape shape1 = new Shape();
            int[] array = { 1,2,3,4};
            shape1.SetArray(array);

            for( int i=0;i< shape1.GetArray().Length;i++)
            {
                    Console.WriteLine(shape1.GetArray()[i]);
            }
    
            Console.WriteLine(shape1.PrintArray());

            Console.ReadKey();


        }

        public static void Print(int a)     // a = 10 ,  a = m
        {
            Console.WriteLine(a);
        }
    }
}
