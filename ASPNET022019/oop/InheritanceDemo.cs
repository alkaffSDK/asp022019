﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.oop
{

    class Mansaf
    {
        public static int someThing;
        public string meat;
        public string rise;

        private void method()
        {
            Creature c = new Creature();
        }
    }
    interface IFlyable
    {
        //void method();
        string CanFly();


    }
    interface IPrintable
    {
       
        string PrintMyType();


    }
    class Creature 
    {
        // to count the number of objects created from this class
        public static int Counter = 0;
        public int AnotherCounter = 0; 
        public int ID { get; set; }
        public Creature()
        {
            AnotherCounter++;
            Mansaf obj = new Mansaf();
            obj.meat = "in object";

            Mansaf.someThing = 10;
            
            Counter++;
            //Console.WriteLine("Creature()");
        }
    }
    class Animal : Creature, IPrintable
    {
        private static int _id = 0;

        public int ID { get; }
        public static int Counter = 0;
        public int AnimalCode { get; set; }
        public Animal()
        {
            ID = ++_id;
            AnotherCounter++;
            Counter++;
            //Console.WriteLine("Animal()");
        }

        public virtual string PrintMyType()
        {
            return "Animal";
        }

        //public  string PrintMyType() { return "Animal"; }
    }

    class Bird :Animal
    {
        public static int Counter = 0;
        private int BirdNumber { get; set; }
        public Bird()
        {
            AnotherCounter++;
            Counter++;
            //Console.WriteLine("Bird()");
        }
        public override string PrintMyType() { return "Bird"; }
    }

    class Mammal :Animal
    {
        public static int Counter = 0;
        public Mammal()
        {
            AnotherCounter++;
            Counter++;
            //Console.WriteLine("Mammal()");
        }
        public override string PrintMyType() { return "Mammal"; }


    }

    class Penguin : Bird
    {
        public static int Counter = 0;
        public Penguin()
        {
            AnotherCounter++;
            Counter++;
            //Console.WriteLine("Penguin()");
        }
        public override string PrintMyType() { return "Penguin"; }

    }

    class Falcon: Bird , IFlyable 
    {
        public static int Counter = 0;
        public Falcon()
        {
            AnotherCounter++;
            Counter++;
            // Console.WriteLine("Falcon()");
        }

     
        public override string PrintMyType() { return "Falcon"; }
      public string CanFly() { return ", it can fly"; }
    }

    class Bat : Mammal, IFlyable
    {
        public static int Counter = 0;
        public Bat()
        {
            AnotherCounter++;
            Counter++;
            // Console.WriteLine("Bat()");
        }

        public override string PrintMyType() { return "Bat"; }
        public string CanFly() { return ", it can fly"; }
    }

    class Cat : Mammal
    {
        public static int Counter = 0;
        public Cat()
        {
            AnotherCounter++;
            Counter++;
            // Console.WriteLine("Cat()");
        }
        public override string PrintMyType() { return "Cat"; }
    }

    class Vehicle : IPrintable
    {
        public virtual string PrintMyType()
        {
            return "Vehicle";
        }
    }
    class Car : Vehicle
    {
        public override string PrintMyType()
        {
            return "Car";
        }
    }

    class Plane : Vehicle, IFlyable
    {
        public string CanFly()
        {
            return "it can fly.";
        }

        public override string PrintMyType()
        {
            return "Plane";
        }
    }
    class InheritanceDemo
    {
        static void Main(string[] args)
        {
            Falcon f = new Falcon();
            Falcon f1 = new Falcon();
            Console.WriteLine(f.AnotherCounter);    // 
            Console.WriteLine(f1.AnotherCounter);    // 
            Bat b = new Bat();
            Console.WriteLine(b.AnotherCounter);

            Bird b1 = new Bird();
            Console.WriteLine(b1.AnotherCounter);

            IFlyable ifly = new Plane();
           
            Falcon c; 
           // Falcon f = new Falcon();        // create 5 objects (object,Creature, Animal, Bird, and Falcon )
            //f.BirdNumber = 10;
            // f.SomeThing = 10;


            object o = new object();        // create only one object

            Random rand = new Random();


            Animal[] animals = new Animal[50];      // 2 objects and 51 variable of type Animal

            for(int i=0;i<animals.Length;i++)
            {
                switch(rand.Next(0,7))
                {
                    case 0:
                        animals[i] = new Falcon();  break;    // 5
                    case 1:
                        animals[i] = new Penguin(); break;    // 5
                    case 2:
                        animals[i] = new Bat();     break;    // 5
                    case 3:
                        animals[i] = new Cat();     break;    // 5
                    case 4:
                        animals[i] = new Bird(); break;      // 4
                    case 5:
                        animals[i] = new Mammal(); break;    // 4
                   default:
                        animals[i] = new Animal(); break;    // 4
                        //animals[i] = new Point();
                        //Object obj = new Point();
                        //Animal a   = new Point();
                }


                /// Wihtout using Interface
                //Console.Write("{0,5} ) {1}",i, animals[i].PrintMyType());
                //// if the varialve is refering to object of type Bat
                //if(animals[i] is Bat)
                //    Console.WriteLine(((Bat)animals[i]).CanFly());
                //// if the varialve is refering to object of type Falcon
                //else if (animals[i] is Falcon)
                //    Console.WriteLine(((Falcon)animals[i]).CanFly());
                //else
                //    Console.WriteLine();

                /// with interface
                /// 
                Console.Write("{0,5} ) {1}", i, animals[i].PrintMyType());
                // if the varialve is refering to object of type IFlyable
                if (animals[i] is IFlyable)
                    Console.WriteLine(((IFlyable)animals[i]).CanFly());
                else
                    Console.WriteLine();

            } // At the end of this loop  we have 51 variable and (200 upto 250) object 

            Console.WriteLine("Objects of type {0,-10} are :{1,3}","Object",animals.Length);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Creature", Creature.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Animal", Animal.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Bird", Bird.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Mammal", Mammal.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Bat", Bat.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Cat", Cat.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Penguin", Penguin.Counter);
            Console.WriteLine("Objects of type {0,-10} are :{1,3}", "Falcon", Falcon.Counter);
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("The total object counter ="+(animals.Length + Creature.Counter+ Animal.Counter + Bird.Counter+ Mammal.Counter+ Bat.Counter+ Cat.Counter+ Penguin.Counter+ Falcon.Counter));


            Console.WriteLine(new Bat().ID);
            
            Console.ReadKey();
        }
    }
}
