﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ASPNET022019.db
{
    class UserLogin
    {
        static void Main(string[] args)
        {
            string connectionString1 = "Server=.\\SQLSERVER;Database=ASP022019;User Id=sa;Password = 123; ";
            // this is a bad way to do sql command 
            string sqlBadWay = "SELECT * FROM [USERS] WHERE [UserLogin] = \'{0}\' AND [UserPassword] = \'{1}\' ; ";


            // here is the good way
            string sql = "SELECT * FROM [USERS] WHERE [UserLogin] = @login AND [UserPassword] = @password ; ";


            using (SqlConnection conn = new SqlConnection(connectionString1))
            using (SqlCommand cmd = new SqlCommand())
            using (SqlCommand cmdProc = new SqlCommand())
            {
                cmd.Connection = conn;
                conn.Open();

                // Store StoredProcedure
                cmdProc.CommandType = CommandType.StoredProcedure;
                cmdProc.CommandText = "AddUser";
                cmdProc.Connection = conn;

                cmdProc.Parameters.AddWithValue("@name", "bbb");
                cmdProc.Parameters.AddWithValue("@age", 22);
                cmdProc.Parameters.AddWithValue("@email", "bbbb");
                cmdProc.Parameters.AddWithValue("@phone", "bbbbb");

                int result = cmdProc.ExecuteNonQuery();
                Console.WriteLine("The result :"+result);



                Console.Write("User name:");
                string login = Console.ReadLine().Trim();

                Console.Write("Password:");
                string pass = Console.ReadLine().Trim();

                // this is the bad way 
                // cmd.CommandText = string.Format(sql, login, pass);

                // here is the good way 
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@login", login);
                cmd.Parameters.AddWithValue("@password", pass);

                // read table 

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    // Read table data
                    while (reader.Read())
                    {
                        // print the all the elements in the values array 
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            Console.Write("{0,10}|", reader.GetValue(i));
                        }
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("The is no data!!!");
                }

            }

            Console.ReadKey();
        }
    }
}
