﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ASPNET022019.db
{
    class DBConnection
    {

        static void Main(string[] args)
        {
            // 1) connection string : 1) server and instance 2) instance 3)Database  4) login (sql authentication user name and password ) or windows authentication 

            string connectionString1 = "Server=.\\SQLSERVER;Database=ASP022019;User Id=sa;Password = 123; ";
            string connectionString2 = @"Server=.\SQLSERVER;Database=ASP022019;  Trusted_Connection = True; ";
            string connectionString3 = @"Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=ASP022019;Data Source=.\SQLSERVER";

            string sqlCommand = "SELECT Count(*) FROM Users ;";
            string sqlCommand1 = "SELECT ID as [User ID], UserName as [User Name] FROM Users ;";
            string sqlCommand2 = "INSERT INTO USERS (UserName , UserAge) values (\'{0}\',{1});";
            // 2) sql connection object 

            using (SqlConnection connection = new SqlConnection(connectionString1))

            // 3) sql command object 

            using (SqlCommand cmdScalar = new SqlCommand(sqlCommand, connection))
            using (SqlCommand cmdReader = new SqlCommand(sqlCommand1, connection))
            using (SqlCommand cmdNonQuery = new SqlCommand())
            {

                // 4) open connection 

                connection.Open();

                // 5) execute the command 

                // 6) get the result 


                // read signle field 
                object result = cmdScalar.ExecuteScalar();
                int MaxID = Convert.ToInt32(result);
                Console.WriteLine("The max id is :" + MaxID);


                // Non query comand 

                Console.Write("Your Name:");
                string name = Console.ReadLine().Trim();

                Console.Write("Your Age:");
                int age = 0;
                while (! int.TryParse(Console.ReadLine().Trim(), out age) |  age <0 |  age > 150)
                {
                    Console.Write("Invalid age value, please try again:");
                }

                cmdNonQuery.CommandText = string.Format(sqlCommand2, name, age);
                cmdNonQuery.Connection = connection;

                int numberOfRows = cmdNonQuery.ExecuteNonQuery();
                Console.WriteLine("The result from the non query :"+ numberOfRows);

                // read table 
                SqlDataReader reader = cmdReader.ExecuteReader();

                // create the holder array to hold the values of each record 
                object[] values = new object[reader.FieldCount];

                // Print the column names 
                for (int i = 0; i < reader.FieldCount; i++)
                    Console.Write(reader.GetName(i) + "\t");
                

                Console.WriteLine("\n------------------------");
                // Read table data
                while (reader.Read())
                {
                    // Fill the record (row) on the values array
                    reader.GetValues(values);
                    // print the all the elements in the values array 
                    for (int i = 0; i < values.Length; i++)
                    {
                        Console.Write(values[i] + "\t");
                    }
                    Console.WriteLine();
                }
               

             

           
            }

            // 7) close connectgion 

           // connection.Dispose();

            Console.ReadKey();

        }
    }
}
