﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ASPNET022019.extra
{
    class FileManager
    {
        private static FileManager instance = new FileManager();

        private static string FilePath ; 

        private FileManager() { }

        public static  FileManager GetInstance(string path) {
            FilePath = path;
            return instance; }

        public void WriteToFile(string data)
        {
            using (StreamWriter sw = new StreamWriter(FilePath))
            {
                sw.WriteLine(data);
            }
        }
    }
}
