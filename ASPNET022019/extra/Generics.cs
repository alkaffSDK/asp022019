﻿using ASPNET022019.oop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019.extra
{
    class Generics
    {
      
        class Pair<K,V>         // Pair<int,string>
        {
            // Key
           public K Key { set; get; }

            // Value
            public V Value { set; get; }
        }

        class Pair
        {
            // Key
            public object Key { set; get; }

            // Value
            public object Value { set; get; }
        }
        static void Main(string[] args)
        {

            Pair p = new Pair();
            p.Key = 1;
            p.Value = "String";


            Pair p1 = new Pair();
            p1.Key = "a";
            p1.Value = new Point();


            Pair p2 = CreatePair<int,string>();
            Pair p3 = CreatePair<string, Point>();

            
            Pair<int,string> p4 = new Pair<int, string>();
            Pair<string, Point> p5 = new Pair<string, Point>();

            Console.WriteLine(Equals<int>(10,15));
            Console.WriteLine(Equals<double>(10.5, 15.5));
            Console.WriteLine(Equals<string>("10.5", "15.5"));
            Console.WriteLine(Equals<Point>(new Point(), new Point()));

        }

        static bool Equals<K>(K a, K b)
        {
            return a.Equals(b);
        }

        static Pair CreatePair<K,V>()
        {
            Pair p = new Pair();
            Random r = new Random();
            switch(r.Next(1,3))
            {
                case 0:
                    p.Key = 1;
                    p.Value = "String";
                    break;
                case 1:
                    p.Key = "string";
                    p.Value = "String";
                    break;
                case 2:
                    p.Key = 'c';
                    p.Value = new Point();
                    break;
            }

            return p;
        }

        
    }
}
