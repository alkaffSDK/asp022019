﻿CREATE TABLE CUSTOMERS( 
   ID   INT identity(1,1)          NOT NULL, 
   NAME VARCHAR (20)     NOT NULL, 
   AGE  INT              NOT NULL, 
   ADDRESS  CHAR (25) , 
   SALARY   DECIMAL (18, 2),        
   PRIMARY KEY (ID));


   Exec sp_columns Users;

   DROP TABLE CUSTOMERS;


SET IDENTITY_INSERT [dbo].[CUSTOMERS] On

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (1, 'Ramesh', 32, 'Ahmedabad', 2000.00 );
  
INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (2, 'Khilan', 25, 'Delhi', 1500.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (3, 'kaushik', 23, 'Kota', 2000.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (4, 'Chaitali', 25, 'Mumbai', 6500.00 ); 
 
INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (5, 'Hardik', 27, 'Bhopal', 8500.00 );  

INSERT INTO CUSTOMERS (ID,NAME,AGE,ADDRESS,SALARY) 
VALUES (6, 'Komal', 22, 'MP', 4500.00 );

SET IDENTITY_INSERT [dbo].[CUSTOMERS] OFF

SELECT NAME FROM CUSTOMERS
where age >= 25 AND Address = 'irbid'; 

SELECT ID, NAME, SALARY FROM CUSTOMERS; 

UPDATE CUSTOMERS 
SET ADDRESS = 'Irbid' 
WHERE Age > 30; 



SELECT * FROM CUSTOMERS order by Age, Name ;

SELECT * FROM CUSTOMERS order by Age DESC , Name;

Select DISTINCT  [Age] from Customers ;

Select TOP 2  Age from Customers order by Age DESC;


select max(Salary) as [Max Salary] from [CUSTOMERS] ;


/****** Script for SelectTopNRows command from SSMS  ******/


Select * from Phones ;



SELECT * from [UserName] as [User Name] , [UserAge] as [Age] , PhoneNumber as [Phone]


Select * from Emails ;
Select * from Users ;
Select * from Emails , Users;
Select * from Emails , Users where [Emails].UserID = [Users].ID;
Select Email, UserName , UserAge from Emails  , Users  where [Emails].UserID = [Users].ID;
Select Email, UserName , UserAge from Emails E , Users U where [E].UserID = [U].ID;
Select Email, UserName , UserAge from Emails E inner join Users U on [E].UserID = [U].ID;
Select Email, UserName , UserAge from Emails E left outer join Users U on [E].UserID = [U].ID;

Select Email, UserName , UserAge from Users U left outer join Emails E on [E].UserID = [U].ID;

Select Email, UserName , UserAge from Emails E right outer join Users U on [E].UserID = [U].ID;

Select Email, UserName , UserAge from Emails E full outer join Users U on [E].UserID = [U].ID;

Select * from Phones ;



SELECT * from [UserName] as [User Name] , [UserAge] as [Age] , PhoneNumber as [Phone]


Select * from Emails ;
Select * from Users ;
Select * from Emails , Users;
Select * from Emails , Users where [Emails].UserID = [Users].ID;
Select Email, UserName , UserAge from Emails  , Users  where [Emails].UserID = [Users].ID;
Select Email, UserName , UserAge from Emails E , Users U where [E].UserID = [U].ID;
Select Email, UserName , UserAge from Emails E inner join Users U on [E].UserID = [U].ID;
Select Email, UserName , UserAge from Emails E left outer join Users U on [E].UserID = [U].ID 

Select Email, UserName , UserAge from Users U left outer join Emails E on [E].UserID = [U].ID

Select Email, UserName , UserAge from Emails E right outer join Users U on [E].UserID = [U].ID;

Select Email, UserName , UserAge from Emails E full outer join Users U on [E].UserID = [U].ID;


select * from vPhones ;
select * from vEmails ;


exec GetAllUsers ;


exec GetUserByID 1 ;

exec SearchInUsers 'alkaff';


exec  AutoCompleteEmail 'a.'