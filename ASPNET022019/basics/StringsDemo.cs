﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class StringsDemo
    {
        // A
        static void Main(string[] args)
        {
            // string : is array of  

            String s = "String";
            string str = "String";

           // str = "strings";

            char[] letters = { 's','t','r','i','n','g'};
            string str1 = new string(letters);

            Console.WriteLine(str);

            Console.WriteLine(str1);

            Console.WriteLine("The letters of the stirng ");
            for (int i=0;i<str.Length;i++)
                Console.WriteLine(str[i]);


            Console.WriteLine("str == str1 ->" + (str == str1));
            Console.WriteLine("str.Equals(str1) ->" + str.Equals(str1));

            Console.WriteLine("str.Equals(str1) ->" + str.Equals(str1,StringComparison.OrdinalIgnoreCase));

            

            Console.WriteLine(str.EndsWith("ing"));

            Console.WriteLine(str.IndexOf('i'));

            int[] arr = {1,2,3,4,5,6,7,8,9,10};
            string value = "{";
            for (int i = 0; i < arr.Length; i++)
            {
                if (i != arr.Length - 1)
                    value =  value.Insert(value.Length, arr[i] + ", ");
                else
                    value = value.Insert(value.Length, arr[i] + "}");
            }


            // {
            // {1,
            // {1,2,
            // {1,2,3,
            // {1,2,3,4,
            // {1,2,3,4,5,
            // {1,2,3,4,5,6,
            // {1,2,3,4,5,6,7,
            // {1,2,3,4,5,6,7,8,
            // {1,2,3,4,5,6,7,8,9,
            // {1,2,3,4,5,6,7,8,9,10};

            Console.WriteLine("using String :" + value.ToString());

            //  StringBuilder

            StringBuilder builder = new StringBuilder("{");

            for (int i = 0; i < arr.Length; i++)
            {
                if (i != arr.Length - 1)
                    builder.Append(arr[i] + ", ");
                else
                    builder.Append(arr[i] + "}");
            }

            
           
            Console.WriteLine("using StringBuilder :" + builder.ToString());

            string n = str.Insert( 0, "new ");

            Console.WriteLine("new Str :"+ n);
            Console.WriteLine(" Str :" + str);


            string input, temp;
            int mid ,  first , last;

            bool IsReversable;
            do
            {
                Console.Write("Your string:");
                input = Console.ReadLine();
                Console.WriteLine("Your input was :" + input);

                temp = input.ToLower().Trim();
                mid = temp.Length / 2;
                 IsReversable = true;

                for (first = 0, last = temp.Length - 1; first < mid; first++, last--)
                {
                    if (temp[first] != temp[last])
                    {
                        IsReversable = false;
                        break;
                    }
                }
                if (IsReversable)
                    Console.WriteLine("The string \'{0}\'  is reversable.", input.Trim());
                else
                    Console.WriteLine("The string \'{0}\'  is not reversable.", input.Trim());

                Console.WriteLine("Press any key to continue or Ecs to exit.");

            } while (! Console.ReadKey().Key.Equals(ConsoleKey.Escape));


            Console.WriteLine("\b Thank you.");
            // abufuba


            Console.ReadKey();

             
        }
    }
}
