﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class ConsoleInput
    {
        static void Main(string[] args)
        {
            int age = 10;
            Console.Write("Your age:");

            // First way
            age = Convert.ToInt32( Console.ReadLine());
            Console.WriteLine("Age: "+ age);

            // Second way
            age = int.Parse(Console.ReadLine());
            Console.WriteLine("Age: " + age);


            // Third way (better)
            bool test = int.TryParse(Console.ReadLine(), out age);
            Console.WriteLine("Test :"+ test);
            Console.WriteLine("Age: " + age);

            double d;
            d = double.Parse(Console.ReadLine());
            Console.WriteLine("D:"+d);

            Console.ReadKey();
        }
    }
}
