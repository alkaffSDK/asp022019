﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{

    class Program
    {
        /// <summary>
        /// Doc new to test the changes
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // this is a single line comment

            /*
              this is a 
              multi line 
              comments 
           */

            //console.writeline("hello\nworld");

            //console.write("line 1");
            //console.writeline("line 2");

            //Console.Write("first line\nsecond line\n");
            //Console.Write("Hello");                        // cout<<"Hello";

            Console.WriteLine(10 + 3 + 5);          // cout<<10 + 3 + 5<<endl;
            Console.WriteLine("10" + 3 + 5);        // 1035
            Console.WriteLine(10 + "3" + 5);        // 1035
            Console.WriteLine(10 + 3 + "5");        // 135
            Console.WriteLine(10 + '3' + 5);        // 66

            Console.ReadKey();

        }
    }
}
