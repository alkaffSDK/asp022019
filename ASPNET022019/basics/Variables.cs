﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    /// [] Optional
    /// <> Mendatory
    /// * : repeated zero or more
    /// + : repeated one or more
    /// | : OR
    class Variables
    {
        // Variables: is a name of a reference for a memory location
        // Syntax :[specifier] [modifier] <data_type> <variable_id> [=<expression>][,<variable_id> [=<expression>]]*;

        static void Main(string[] args)
        {

            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("You entered :"+a);

            //int a = 10 + 25, m = 125 , e ;
            //int x = 10;
            //int c = 125487 ;


            //bool boolean = false;
            //int ui = 10;           // [U]
            //Console.WriteLine(10U);
            //long i = 10L;           // [L]
            //Console.WriteLine(10L);
            //ulong ul = 10lu;        // [LU|UL|U|L]

            //float f = 11.123456789101112131415F;            // F
            //double d = 11.123456789101112131415;            // [D]
            //decimal dec = 11.123456789101112131415M;         // M

            //Console.WriteLine("F: "+f);
            //Console.WriteLine("D: " + d);
            //Console.WriteLine("Dec: " + dec);

            //char ch = 'a';
            //Console.WriteLine("Ch: " + ch);
            //Console.WriteLine("A :" + 3 + 'a');     // A :3a
            //Console.WriteLine("A :"+ (3 +"a"));     // A :3a
            //Console.WriteLine("A :" + (3 + 'a'));   // A :100


            //// Type conversion
            //short sh = (short)c;
            //Console.WriteLine("sh: " + sh);
            //x =  (int)f;
            //Console.WriteLine("x: " + x);

            //x = Convert.ToInt32(f);     // I need to convert f to int

            //Console.WriteLine(Convert.ToBoolean("false"));      // false
            //Console.WriteLine(Convert.ToBoolean(false));        // false
            //Console.WriteLine(Convert.ToBoolean(15));           // true


            //string s = x.ToString();
            //s = Convert.ToString(x);

            //Console.ReadKey();

        }
    }
}
