﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class TaskMaxAndMin
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int[] marks = new int[1000];
            double sum = 0;
            int max = int.MinValue, max1 = int.MinValue, max2 = int.MinValue;
            int min = int.MaxValue, min1 = int.MaxValue, min2 = int.MaxValue;

            int maxCounter = 0, max1Counter = 0, max2Counter = 0;
            int minCounter = 0, min1Counter = 0, min2Counter = 0;


            Console.Write("{");
            for (int i = 0; i < marks.Length; i++)
            {
                marks[i] = rand.Next(35, 101);
                // To print
                Console.Write(marks[i] + ", ");
                // sum 
                sum += marks[i];
                if (marks[i] == max) maxCounter++;
                else if (marks[i] == max1) max1Counter++;
                else if (marks[i] == max2) max2Counter++;
                else if (marks[i] == min) minCounter++;
                else if (marks[i] == min1) min1Counter++;
                else if (marks[i] == min2) min2Counter++;
                else
                {
                    if (marks[i] > max)
                    {
                        max2 = max1;
                        max2Counter = max1Counter;
                        max1 = max;
                        max1Counter = maxCounter;
                        max = marks[i];
                        maxCounter = 1;

                    }
                    else if (marks[i] > max1)
                    {
                        max2 = max1;
                        max2Counter = max1Counter;
                        max1 = marks[i];
                        max1Counter = 1;

                    }
                    else if (marks[i] > max2)
                    {
                        max2 = marks[i];
                        max2Counter = 1;
                    }
                    else if (marks[i] < min)
                    {
                        min2 = min1;
                        min2Counter = min1Counter;
                        min1 = min;
                        min1Counter = minCounter;
                        min = marks[i];
                        minCounter = 1;
                    }
                    else if (marks[i] < min1)
                    {
                        min2 = min1;
                        min2Counter = min1Counter;
                        min1 = marks[i];
                        min1Counter = 1;
                    }
                    else if (marks[i] < min2)
                    {
                        min2 = marks[i];
                        min2Counter = 1;
                        
                    }
                }

            }
            Console.Write("\b\b}\n");

            Console.WriteLine("The max are :" + max + ":(" + maxCounter + ") , " + max1 + ":(" + max1Counter + ") , " + max2 + ":(" + max2Counter + ")");
            Console.WriteLine("The max are :{0,4}:({1,5}) , {2,4}:({3}) , {4,4}:({5})", max, maxCounter, max1, max1Counter, max2, max2Counter);

            Console.WriteLine("The max  :{0,3} :({1})", max, maxCounter);
            Console.WriteLine("The max1 :{0,3} :({1})", max1, max1Counter);
            Console.WriteLine("The max2 :{0,3} :({1})", max2, max2Counter);
            Console.WriteLine("The min2 :{0,3} :({1})", min2, min2Counter);
            Console.WriteLine("The min1 :{0,3} :({1})", min1, min1Counter);
            Console.WriteLine("The min  :{0,3} :({1})", min, minCounter);
            Console.WriteLine("Average :" + (sum / marks.Length));
            Console.ReadKey();
        }
    }
}
