﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class Operators
    {
        static void Main(string[] args)
        {
            int a = 10, b = 3;
            // + , -, / , * , % , 


            Console.WriteLine(10 % 3 );         // 1
            Console.WriteLine(10 % 3.5);        // 3
            Console.WriteLine(10.5 % 4);        // 2.5
            //  ++, -- , (-) , (+)   unary operators 
            Console.WriteLine(a++);     // 10
            Console.WriteLine(a);       // 11 
            Console.WriteLine(++a);     // 12

            Console.WriteLine(a--);     // 12
            Console.WriteLine(a);       // 11
            Console.WriteLine(--a);     // 10


            // && , || , !  Logical Operators  (boolean)

            //  Console.WriteLine(a && b);      // Error: && only accepts boolean operands
            Console.WriteLine(a> 0 && b!= 8);       // true
            Console.WriteLine(true || false);       // true
            Console.WriteLine(!true);               // false
            // == , != , >, < , >=, <=  aceptes numbers and return boolean

            Console.WriteLine(a == 15);     // is a equale to 15 // false
            Console.WriteLine(a = 15 );     // put 15 in a // 15
            Console.WriteLine("A:"+a);      // 15
            // = , += , *= , -= , %= , /= , . . . . 
            a = 10;     // put 10 in a

            a *= 2 + 3;    // a = a * (2 + 3)
            Console.WriteLine("A:" + a);      // 50
            a += 2 + 3;
            Console.WriteLine("A:" + a);        //  35

            a = 10;
            a *= 3 + 2;     // a = a * (3+2)
            Console.WriteLine("A:" + a); //  50 NOT 32 

            // Priority
            a = 2;
            b = 3;
            int c = 3 * a + 2 * b * (a + b) / 3;
            // a = 2 , b = 3
            // c = 16;
            Console.WriteLine("First C:" + c);          // 16

            // a += 2;
            // a = a + 2;
            // a += a;

            c = 3 * ++a + 2 * b++ * (++a + ++b) / 3;
            // a = 4 , b = 5
            // c = 27;
            // a = 4 , b = 5
            // c = 27;

            Console.WriteLine("Second C:" + c);         // 



            a = 10;     // 1010
            b = 7;      // 0111

            // ^ , &, | , ~ , << , >> 


            // & (bitwise AND) ,  boolean or integer  --> boolean or integer
            // Console.WriteLine(a && b);
            Console.WriteLine("a & b :"+ (a & b));           // 2
                                                // a =  1010  (10)
                                                // b =  0111   (7) 
                                                //--------------------- &
                                                //      0010    (2) 

            Console.WriteLine(true & false);           // true && false  // false
            Console.WriteLine(true && false);           // true && false  // false


            // | (bitwise OR) , 
            Console.WriteLine("a | b :" + (a | b));           // 15
                                                              // a =  1010  (10)
                                                              // b =  0111   (7) 
                                                              //--------------------- |
                                                              //      1111   (15)

            // ^ (bitwise XOR) , 
            Console.WriteLine("a ^ b :" + (a ^ b));           // 13
                                                              // a =  1010  (10)
                                                              // b =  0111   (7) 
                                                              //--------------------- ^
                                                              //      1101   (13)

            // ~ (bitwise 1's complement) , 
            Console.WriteLine(~a);              // -11
                                                // a =  000001010  (10)
                                                //--------------------- ~
                                                //      1111110101   (-11) -
                                                //  1111000111100

            // 00000000000000000000000000001010
            // 11111111111111111111111111110101
            // 00000000000000000000000000001011
            // 1111 1111 1111 1111 1001 0010 0100‬
            // 1111 1111 1111 1111 1001 0010 0100
            // 011011011100
            // 11 1111 1111 1111 1001 0010 010000
            Console.WriteLine("<< (shift left)  ");
            // << (shift left) 
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})","A", a, string.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 0", a << 0, string.Format("{0,32}", Convert.ToString(a << 0, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 1", a << 1, string.Format("{0,32}", Convert.ToString(a << 1, 2)).Replace(" ", "0")));          // 20
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 2", a << 2, string.Format("{0,32}", Convert.ToString(a << 2, 2)).Replace(" ", "0")));          // 40
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 3", a << 3, string.Format("{0,32}", Convert.ToString(a << 3, 2)).Replace(" ", "0")));          // 80


            a = -10;
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "A", a, string.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 0", a << 0, string.Format("{0,32}", Convert.ToString(a << 0, 2)).Replace(" ", "0")));          // -10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 1", a << 1, string.Format("{0,32}", Convert.ToString(a << 1, 2)).Replace(" ", "0")));          // -20
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 2", a << 2, string.Format("{0,32}", Convert.ToString(a << 2, 2)).Replace(" ", "0")));          // -40
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a << 3", a << 3, string.Format("{0,32}", Convert.ToString(a << 3, 2)).Replace(" ", "0")));          // -80


            a = 10;
            Console.WriteLine(">> (shift right)  ");
            // << (shift right) 
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "A", a, string.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 0", a >> 0, string.Format("{0,32}", Convert.ToString(a >> 0, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 1", a >> 1, string.Format("{0,32}", Convert.ToString(a >> 1, 2)).Replace(" ", "0")));          // 20
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 2", a >> 2, string.Format("{0,32}", Convert.ToString(a >> 2, 2)).Replace(" ", "0")));          // 40
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 3", a >> 3, string.Format("{0,32}", Convert.ToString(a >> 3, 2)).Replace(" ", "0")));          // 80


            a = -10;
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "A", a, string.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0")));          // 10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 0", a >> 0, string.Format("{0,32}", Convert.ToString(a >> 0, 2)).Replace(" ", "0")));          // -10
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 1", a >> 1, string.Format("{0,32}", Convert.ToString(a >> 1, 2)).Replace(" ", "0")));          // -20
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 2", a >> 2, string.Format("{0,32}", Convert.ToString(a >> 2, 2)).Replace(" ", "0")));          // -40
            Console.WriteLine(string.Format("{0,10} ->{1}:({2})", "a >> 3", a >> 3, string.Format("{0,32}", Convert.ToString(a >> 3, 2)).Replace(" ", "0")));          // -80
            Console.WriteLine(string.Format("{0}:{1}", a << 1, string.Format("{0,32}", Convert.ToString((a << 1), 2)).Replace(" ", "0")));          // 20
                                                                                                                                  // a =  0000000000000000000000000001010  (10)
                                                                                                                                  //--------------------- << 1
                                                                                                                                  // a =  0000000000000000000000000010100  (20)


            Console.WriteLine((a << 2) + ":" + string.Format("{0,32}", Convert.ToString((a << 2), 2)).Replace(" ", "0"));          // 20
                                                                                            // a =  0000000000000000000000000101000  (40)

            // a is 32 bits
            a = -38;
            Console.WriteLine("A :"+(a) + ":" + string.Format("{0,32}", Convert.ToString((a), 2)).Replace(" ", "0"));          // 20

            Console.WriteLine((a << 1) + ":" + string.Format("{0,32}", Convert.ToString((a << 1), 2)).Replace(" ", "0"));          // 20
                                                                                                                                   // a =  11111111111111111111111111011010  (-38)
                                                                                                                                   //--------------------- << 1
                                                                                                                                   // a =  11111111111111111111111110110100  (-76)

            Console.WriteLine((a << 2) + ":" + string.Format("{0,32}", Convert.ToString((a << 2), 2)).Replace(" ", "0"));          // 20
                                                                                                                                   // a =  11111111111111111111111111011010  (-38)
                                                                                                                                   //--------------------- << 2
                                                                                                                                   // a =  11111111111111111111111101101000  (-152)

            sbyte d = -38;
            // d is 8 bits  
            Console.WriteLine("D :" + (d) + ":" + string.Format("{0,8}", Convert.ToString((d), 2)).Replace(" ", "0"));          // 20

            Console.WriteLine((d << 1) + ":" + string.Format("{0,8}", Convert.ToString((d << 1), 2)).Replace(" ", "0"));          // 20
                                                                                                                                  // d =  11011010  (-38)
                                                                                                                                  //--------------------- << 1
                                                                                                                                  // d =  10110100  (-76)

            Console.WriteLine((d << 2) + ":" + string.Format("{0,8}", Convert.ToString((d << 2), 2)).Replace(" ", "0"));          // 20
                                                                                                                                  // d =  11011010  (-38)
                                                                                                                                  //--------------------- << 2
                                                                                                                                  // d =  01101000  (106)
            Console.WriteLine(">> (shift right)  ");
            // 
            Console.WriteLine("A :" + (a) + ":" + string.Format("{0,32}", Convert.ToString((a), 2)).Replace(" ", "0"));          // 20
            Console.WriteLine((a << 1) + ":" + string.Format("{0,32}", Convert.ToString((a << 1), 2)).Replace(" ", "0"));          // 20
                                                                                                                                   // a =  0000000000000000000000000001010  (10)
                                                                                                                                   //--------------------- << 1
                                                                                                                                   // a =  0000000000000000000000000010100  (20)


            Console.WriteLine((a << 2) + ":" + string.Format("{0,32}", Convert.ToString((a << 2), 2)).Replace(" ", "0"));          // 20
                                                                                                                                   // a =  0000000000000000000000000001010  (10)
                                                                                                                                   //--------------------- << 2
                                                                                                                                   // a =  0000000000000000000000000101000  (40)

            // a is 32 bits
            a = -38;
            Console.WriteLine("A :" + (a) + ":" + string.Format("{0,32}", Convert.ToString((a), 2)).Replace(" ", "0"));          // 20

            Console.WriteLine((a << 1) + ":" + string.Format("{0,32}", Convert.ToString((a << 1), 2)).Replace(" ", "0"));          // 20

            Console.WriteLine((a << 2) + ":" + string.Format("{0,32}", Convert.ToString((a << 2), 2)).Replace(" ", "0"));          // 20
      
             d = -38;
            // d is 8 bits  
            Console.WriteLine("D :" + (d) + ":" + string.Format("{0,8}", Convert.ToString((d), 2)).Replace(" ", "0"));          // 20

            Console.WriteLine((d << 1) + ":" + string.Format("{0,8}", Convert.ToString((d << 1), 2)).Replace(" ", "0"));          // 20
                                                                                                                                  // d =  11011010  (-38)
                                                                                                                  // d =  10110100  (-76)

            Console.WriteLine((d << 2) + ":" + string.Format("{0,8}", Convert.ToString((d << 2), 2)).Replace(" ", "0"));          // 20
                                                                                                                                  // d =  11011010  (-38)
                                                                                                                                  //--------------------- << 2

            a = 10;
            Console.WriteLine(a < 5 && a++ > 2);       // false
            Console.WriteLine("A:" + a);                // 10 

            a = 10;
            Console.WriteLine(a < 5 & a++ > 2);       // false 
            Console.WriteLine("A:" + a);              // 11

            Console.WriteLine(Math.Pow(2,3));
            Console.ReadKey();
        }
    }
}
