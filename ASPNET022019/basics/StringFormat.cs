﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class StringFormat
    {
        static void Main(string[] args)
        {
            int p1 = 10, p2 = 153, p3 = 1521552;

            Console.WriteLine("Item 1 :"+ p1+ " JD");
            Console.WriteLine("Item 2 :" + p2 + " JD");
            Console.WriteLine("Item 3 :" + p3 + " JD");
            Console.WriteLine("-------------");
            Console.WriteLine("Item 1 :{0,-10} {1,-5}",p1,"JD");
            Console.WriteLine("Item 2 :{0,10} {1,-5}", p2, "JD");
            Console.WriteLine("Item 3 :{0,10} {1,-5}", p3, "JD");


            int a = 10;


            Console.WriteLine(a);                       // 10
            Console.WriteLine("{0,32}",Convert.ToString(a,2));   // 1010
            Console.WriteLine( String.Format("{0,32}", Convert.ToString(a, 2)).Replace(" ", "0"));
           ;

            a = -10;

            Console.WriteLine(a);                       // 10
            Console.WriteLine("{0,32}", Convert.ToString(a, 2));   // 1010


            Console.ReadKey();
        }
    }
    
}
