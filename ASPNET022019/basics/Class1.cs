﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPNET022019
{
    class Class1
    {
        static void Main(string[] args)
        {
            Random ran = new Random();
            int[,,,] arr = new int[4, 3, 5, 2];

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int x = 0; x < arr.GetLength(1); x++)
                {
                    for (int y = 0; y < arr.GetLength(2); y++)
                    {
                        for (int z = 0; z < arr.GetLength(3); z++)
                        {
                            arr[i, x, y, z] = ran.Next(35, 101);
                            Console.Write(arr[i, x, y, z] + ", ");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine("-----------------");
                }
                Console.WriteLine("############################");
            }


            Console.ReadKey();
        }
    }

}
