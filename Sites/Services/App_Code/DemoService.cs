﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for DemoService
/// </summary>
[WebService(Namespace = "http://sdkjordan.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.None)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class DemoService : System.Web.Services.WebService
{

    public DemoService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(MessageName = "HelloWorld")]
    public string HelloWorld()
    {
        
        return "Hello World";
    }
    [WebMethod(MessageName= "HelloWorld1", EnableSession = true )]
    public string HelloWorld(int c)
    {
        Session["counter"] = c; 
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < c; i++)
        {
            sb.Append("Hello World").Append(",");
        }
        return sb.ToString().Substring(0, sb.Length - 1);
    }

    [WebMethod(Description ="Print hello then the name")]
    public string SayHello(string name)
    {
        return "Hello "+ name;
    }

    [WebMethod]
    public string Test(string name)
    {
        return "Hello " + name;
    }

    [WebMethod]
    public int Add(int a, int b )
    {
        return a + b ;
    }

    [WebMethod (MessageName = "GetTermpByCityName")]
    public int GetTermp(string city)
    {
        return 0;
    }

    [WebMethod(MessageName = "GetTermpByCityCode" ,CacheDuration = 60)]
    public int Gettermp(int code)
    {
        return 0;
    }

}
