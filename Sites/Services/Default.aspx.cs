﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        using (DemoServiceReference.DemoServiceSoapClient client = new DemoServiceReference.DemoServiceSoapClient())
        {
            client.Open();
            Label1.Text = client.HelloWorld();
            Label2.Text = client.SayHello(TextBox1.Text);
        }
    }
}