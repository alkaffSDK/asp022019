﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebSite18.Startup))]
namespace WebSite18
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
