﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Source : System.Web.UI.Page
{

    public String MyText { get { return TextBox1.Text; } }
    protected void Page_Load(object sender, EventArgs e)
    {
     
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if(sender is LinkButton)
        {
            LinkButton but = (LinkButton)sender;
            switch(but.ClientID)
            {
                case "LinkButton1":
                    Response.Redirect(@"~/Target.aspx?name="+Server.HtmlEncode(TextBox1.Text)+"&Age="+ Server.HtmlEncode("36"), false);
                    break;
                case "LinkButton2":
                    Response.Redirect(@"http:\\www.sdkjordan.com", false);
                    break;
            }
        }    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if(sender is Button)
        {
            Button btn = (Button)sender;

            switch(btn.ClientID)
            {
                case "Button1":
                    Server.Transfer(@"~/Target.aspx");
                    break;
                case "Button2":
                    Server.Execute(@"~/Target.aspx");
                    break;
            }
        }
       
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        // this will not be executec
        Response.Redirect(@"http:\\www.sdkjordan.com", false);
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Write("<script>");
        Response.Write("window.open('Target.aspx','_blank')");
        Response.Write("</script>");
      
       
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
}