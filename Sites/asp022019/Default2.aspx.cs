﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ViewState["counter"] != null)
                TextBox1.Text = ViewState["counter"].ToString();
            else
                TextBox1.Text = "No view state";

            if (Request.Cookies["counter"] != null)
                TextBox2.Text = Request.Cookies["counter"].Value.ToString();
            else
                TextBox2.Text = "No cookie";

        }
    }
}