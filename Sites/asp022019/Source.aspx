﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Source.aspx.cs" Inherits="Source" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
        <br />
        <asp:HyperLink ID="HyperLinkInternal" runat="server" NavigateUrl="~/Target.aspx" >Internal Hyberlink</asp:HyperLink>&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLinkExternal" runat="server" Target="_blank" NavigateUrl="http:\\sdkjordan.com/" >External Hyberlink</asp:HyperLink> <br />
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Response Redirect</asp:LinkButton> &nbsp;&nbsp;
         <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton1_Click">External Response Redirect</asp:LinkButton>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Server Transfer" OnClick="Button1_Click"  Width="30%"/>
        <br />
        <asp:Button ID="Button2" runat="server" Text="Server Execute" OnClick="Button1_Click" Width="30%"/>
        <br />
        <asp:Button ID="Button3" runat="server" Text="Cross-Page Postback" PostBackUrl="~/Target.aspx" Width="30%" OnClick="Button3_Click" />
        <br />
        <asp:Button ID="Button4" runat="server" Text="Window open" Width="30%" OnClick="Button4_Click" />


    </div>
    </form>
</body>
</html>
