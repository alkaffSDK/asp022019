﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestCookies : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Browser.Cookies)
            { // Browser is supporting cookies  
                if (Request.QueryString["re"] == null) // this is the first request
                {
                    Response.Cookies["cookies"].Value = "Yes";
                    Response.Cookies["cookies"].Expires = DateTime.Now.AddSeconds(10);
                    Response.Redirect(Request.RawUrl + "?re=1");
                }
                else
                {// this is the redirected request
                    if (Request.Cookies["cookies"] != null)
                    {// Cookies are enabled 
                        LabelCookiesState.Text = "Cookies are enabled";
                        LabelCookiesState.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {// Cookies are disabled 
                        LabelCookiesState.Text = "Cookies are disabled";
                        LabelCookiesState.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            else
            {
                // Browser is NOT supporting cookies  
                LabelCookiesState.Text = "Cookies are not supported by the browser";
                LabelCookiesState.ForeColor = System.Drawing.Color.Orange;
            }
        }

    }

}