﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"  EnableViewState="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <input id="TextHTML" type="text" runat="server" />
            <input id="ButtonHTML" type="button" value="ButtonHTML" />
            <br />
            <asp:TextBox ID="TextBoxASP" runat="server"></asp:TextBox>
            <asp:Button ID="ButtonASP" runat="server" Text="ButtonASP" OnClick="ButtonASP_Click" />
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
                 <asp:ListItem Text="A" Value="1"></asp:ListItem>
                <asp:ListItem Text="B" Value="2"></asp:ListItem>
                <asp:ListItem Text="C" Value="3"></asp:ListItem>


            </asp:DropDownList>
            <input  hidden="hidden"/>
        </div>
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
         <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
          <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
         <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
        <asp:Button ID="ButtonCounter" runat="server" Text="Counter" OnClick="ButtonCounter_Click" />
        <br />
       <asp:Button ID="ButtonGo" runat="server" Text="Go" OnClick="ButtonGo_Click"  />
         <br />
           <asp:Label ID="Label1" runat="server" Text="ViewState['Counter']"></asp:Label>
        <asp:TextBox ID="TextBox4" runat="server" ></asp:TextBox>
        <br />
         <asp:Label ID="Label2" runat="server" Text="Cookie['Counter']"></asp:Label>
        <asp:TextBox ID="TextBox6" runat="server" ></asp:TextBox>
        <div>
        </div>
    </form>
</body>
</html>
