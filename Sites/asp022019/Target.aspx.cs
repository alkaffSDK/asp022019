﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Target : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(! IsPostBack)
        {
            TextBox1.Text = "==================== QueryString ====================\n";
            TextBox1.Text += "Name:" +Server.HtmlDecode(Request.QueryString["name"]) + ", Age:"+ Server.HtmlDecode(Request.QueryString["age"]+"\n");
            //TextBox1.Text += "==================== Form collection ====================\n";
            //foreach(string k in  Request.Form.AllKeys)
            //{
            //    if(! k.StartsWith("__"))
            //        TextBox1.Text += string.Format("{0,-20}:{1}\n", k, Request.Form[k]);
            //}
            if(PreviousPage != null)
            {
                TextBox1.Text += "==================== PreviousPage ====================\n";
                TextBox text = PreviousPage.FindControl("TextBox85") as TextBox ;
                if(text != null)
                    TextBox1.Text += text.Text+ "\n";

                TextBox1.Text += "==================== Public Properity ====================\n";
                TextBox1.Text += PreviousPage.MyText+"\n";
            }

            TextBox1.Text += "==================== Request.Form ====================\n";
            foreach (var k in Request.Form.AllKeys)
            {
                TextBox1.Text += string.Format("{0,20} : {1}\n", k, Request.Form[k]);
            }
           
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        // this will not be executec
        Response.Redirect(@"http:\\www.sdkjordan.com", false);
    }
}