﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        // first method
        //TextBox tb = Master.FindControl("TextBox1") as TextBox;
        //if(tb != null)
        //    TextBox1.Text = tb.Text;

        // second method : public proprity

        //TextBox1.Text = ((MyMasterPage)Master).MyPublicProperty;

        // third method : <%@ MasterType VirtualPath="~/MyMasterPage.master" %>

        TextBox1.Text = Master.MyPublicProperty;

    }
}