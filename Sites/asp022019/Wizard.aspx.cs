﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Wizard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        if (Wizard1.ActiveStepIndex == Wizard1.WizardSteps.Count - 1)
        {
            LabelName.Text = TextBoxName.Text;
            LabelPhone.Text = TextBoxPhone.Text;
            LabelEmail.Text = TextBoxEmail.Text;
            LabelAge.Text = TextBoxAge.Text;
            LabelAddress.Text = TextBoxAddress.Text;
            LabelBirthDate.Text = TextBoxBirthDate.Text;
        }
    }

    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        // save the data 
    }
}