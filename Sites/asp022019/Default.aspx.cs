﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["counter"] != null)
            TextBox4.Text = ViewState["counter"].ToString();
        else
            TextBox4.Text = "No view state";

        if (Request.Cookies["counter"] != null)
            TextBox6.Text = Request.Cookies["counter"].Value.ToString();
        else
            TextBox6.Text = "No cookie";



        if ( !IsPostBack)
        {
            DropDownList1.Items.Insert(0, new ListItem("Please select an item"));

            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox5.Text = "0";
        }
    }

    protected void ButtonASP_Click(object sender, EventArgs e)
    {
        //TextBoxASP.Text = "Button is clicked!";
        //TextHTML.Value = "changed";

        if(! string.IsNullOrWhiteSpace(TextHTML.Value) && TextHTML.Value.Length >1)
            DropDownList1.Items.Add(new ListItem(TextHTML.Value, (DropDownList1.Items.Capacity +1).ToString()));
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBoxASP.Text = DropDownList1.SelectedItem + ":" + DropDownList1.SelectedValue;
    }

    int testCounter = 0;
    protected void ButtonCounter_Click(object sender, EventArgs e)
    {
        int counter1 = 0;
        int.TryParse(TextBox3.Text, out counter1);
        counter1++;
        TextBox3.Text = counter1.ToString();

        testCounter++;
        TextBox1.Text = testCounter.ToString();

        int counter =0;
       
        if (ViewState["counter"] != null)
            int.TryParse(ViewState["counter"].ToString(), out counter);

            counter++;
            TextBox2.Text = counter.ToString();
            ViewState["counter"] = counter;


        int counter2 = 0;
        if(Request.Cookies["counter"] != null)
            int.TryParse(Request.Cookies["counter"].Value.ToString(), out counter2);

        counter2++;
        TextBox5.Text = counter2.ToString();
        Response.Cookies["counter"].Value = counter2.ToString();
        Response.Cookies["counter"].Expires = DateTime.Now.AddHours(1);

    }

    protected void ButtonGo_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Default2.aspx");
    }
}