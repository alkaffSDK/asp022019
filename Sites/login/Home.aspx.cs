﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label1.Text = "";
        //foreach (TableCell c in GridView1.SelectedRow.Cells)
        //    Label1.Text += c.Text + ":";

        Label1.Text = GridView1.SelectedRow.Cells[1].Text;
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
    }

    protected void LinkButtonInsert_Click(object sender, EventArgs e)
    {
        TextBox tb = GridView1.FooterRow.FindControl("TextBoxFooterName") as TextBox;
        if (tb != null)
            SqlDataSource1.InsertParameters["NAME"].DefaultValue = tb.Text;

        tb = GridView1.FooterRow.FindControl("TextBoxFooterAge") as TextBox;
        if (tb != null)
            SqlDataSource1.InsertParameters["Age"].DefaultValue = tb.Text;

        tb = GridView1.FooterRow.FindControl("TextBoxFooterAddress") as TextBox;
        if (tb != null)
            SqlDataSource1.InsertParameters["ADDRESS"].DefaultValue = tb.Text;

        tb = GridView1.FooterRow.FindControl("TextBoxFooterSalary") as TextBox;
        if (tb != null)
            SqlDataSource1.InsertParameters["SALARY"].DefaultValue = tb.Text;

        SqlDataSource1.Insert();
    }
}