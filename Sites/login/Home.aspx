﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/fontawesome.css" rel="stylesheet" />
    <link href="css/brands.css" rel="stylesheet" />
    <link href="css/solid.css" rel="stylesheet" />
    <style>
        body {
            position: relative;
        }

        .possition {
            position: relative;
            background: no-repeat center center;
            width: 20px;
            height: 20px;
            border: none;
            margin-left: -25px;
            top: 3px;
            z-index: 999;
        }

        .text {
            padding-right: 22px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:TextBox ID="TextBox1" runat="server" CssClass="text" Height="30px"></asp:TextBox>
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/search.svg" CssClass="possition" OnClick="ImageButton1_Click" />

            <br />
            <asp:GridView ID="GridView1" Width="80%" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" PageSize="5" DataKeyNames="ID" ForeColor="#333333" GridLines="None" ShowFooter="True">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="LinkButtonInsert" runat="server" OnClick="LinkButtonInsert_Click">Insert</asp:LinkButton>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  ForeColor="Red" DisplayMode="SingleParagraph" Font-Size="Medium" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select" Text="Select"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" ReadOnly="True"></asp:BoundField>
                    <asp:TemplateField HeaderText="NAME" SortExpression="NAME">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NAME") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="TextBoxFooterName" runat="server" Width="90%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ControlToValidate="TextBoxFooterName" Text="*" ErrorMessage="Name is required." ForeColor="Red"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AGE" SortExpression="AGE">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AGE") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("AGE") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="TextBoxFooterAge" runat="server" Width="90%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAge" runat="server" ControlToValidate="TextBoxFooterAge" Text="*" ErrorMessage="Age is required." ForeColor="Red"></asp:RequiredFieldValidator>

                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ADDRESS" SortExpression="ADDRESS">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="TextBoxFooterAddress" runat="server" Width="90%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddress" runat="server" ControlToValidate="TextBoxFooterAddress" Text="*" ErrorMessage="Address is required." ForeColor="Red"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SALARY" SortExpression="SALARY">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("SALARY") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("SALARY") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="TextBoxFooterSalary" runat="server" Width="90%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSalary" runat="server" ControlToValidate="TextBoxFooterSalary" Text="*" ErrorMessage="Salary is required." ForeColor="Red"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="Yellow" />
                <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>


        <asp:SqlDataSource ID="SqlDataSource1" runat="server"
            ConnectionString="<%$ ConnectionStrings:ASP2018DBConnectionString %>"
            SelectCommand="SELECT [ID],[NAME], [AGE], [ADDRESS], [SALARY] FROM [CUSTOMERS] WHERE (([NAME] LIKE '%' + @Data + '%') OR ([ADDRESS] LIKE '%' + @Data + '%') OR ( [AGE] LIKE @Data))"
            DeleteCommand="DELETE FROM [CUSTOMERS] WHERE [ID] = @ID"
            InsertCommand="INSERT INTO [CUSTOMERS] ([NAME], [AGE], [ADDRESS], [SALARY]) VALUES (@NAME, @AGE, @ADDRESS, @SALARY)"
            UpdateCommand="UPDATE [CUSTOMERS] SET [NAME] = @NAME, [AGE] = @AGE, [ADDRESS] = @ADDRESS, [SALARY] = @SALARY WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                 <asp:Parameter Name="NAME" Type="String" />
                <asp:Parameter Name="AGE" Type="Int32" />
                <asp:Parameter Name="ADDRESS" Type="String" />
                <asp:Parameter Name="SALARY" Type="Decimal" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="NAME" Type="String" />
                <asp:Parameter Name="AGE" Type="Int32" />
                <asp:Parameter Name="ADDRESS" Type="String" />
                <asp:Parameter Name="SALARY" Type="Decimal" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
            <SelectParameters>
                <asp:ControlParameter Name="Data" ControlID="TextBox1" DbType="String" DefaultValue="%" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
