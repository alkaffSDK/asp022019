﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Required meta tags-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Colorlib Templates" />
    <meta name="author" content="Colorlib" />
    <meta name="keywords" content="Colorlib Templates" />

    <!-- Title Page-->
    <title>Registation</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all" />
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all" />
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" />

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all" />
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all" />

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="page-wrapper bg-red p-t-180 p-b-100 font-robo">
            <div class="wrapper wrapper--w960">
                <div class="card card-2">
                    <div class="card-heading"></div>
                    <div class="card-body">
                        <h2 class="title">Registration Info</h2>
                        <div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxFistName" runat="server" CssClass="input--style-2" placeholder="First Name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="First name is required." ControlToValidate="TextBoxFistName" ForeColor="Red">*</asp:RequiredFieldValidator>
                                    </div>


                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBoxLastName" runat="server" CssClass="input--style-2" placeholder="Last Name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Last name is required." ControlToValidate="TextBoxLastName" ForeColor="Red">*</asp:RequiredFieldValidator>

                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="input--style-2" placeholder="Email" TextMode="Email"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Email is required." ControlToValidate="TextBoxEmail" ForeColor="Red">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="TextBoxPhone" runat="server" CssClass="input--style-2" placeholder="Phone" TextMode="Phone"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="phone number is required." ControlToValidate="TextBoxPhone" ForeColor="Red" Display="Dynamic">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid mobile number." ControlToValidate="TextBoxPhone" ForeColor="Red" Display="Dynamic" ValidationExpression="^0?7[7|8|9]\d{7}$">*</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="TextBoxBirthDate" runat="server" CssClass="input--style-2 js-datepicker" placeholder="Birthdate"></asp:TextBox>
                                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Birth date is required." ControlToValidate="TextBoxBirthDate" ForeColor="Red">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-2">

                                        <div class="input-group" style="display: inline-flex">
                                            <asp:RadioButton ID="RadioButton1" runat="server" Text="Male" GroupName="gender" CssClass="input--style-2" ValidateRequestMode="Enabled" />
                                            <asp:RadioButton ID="RadioButton2" runat="server" Text="Female" GroupName="gender" CssClass="input--style-2" ValidateRequestMode="Enabled" />
                                        </div>

                                    </div>
                                </div>
                                <div class="input-group" style="display: inline-block">

                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="rs-select2 js-select-simple select--no-search" DataSourceID="SqlDataSource1" DataTextField="CountryName" DataValueField="CountryId" OnDataBound="DropDownList1_DataBound">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CountriesConnectionString %>" SelectCommand="SELECT [CountryId], [CountryName] FROM [Country] ORDER BY [CountryName]"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please select a country" ForeColor="Red" ControlToValidate="DropDownList1" InitialValue="-1">*</asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="row row-space">
                                <div class="col-2">
                                      <div class="p-t-30">
                                    <asp:Button ID="ButtonRegister" runat="server" Text="Register" CssClass="btn btn--radius btn--green" OnClick="ButtonRegister_Click" />
                                </div>
                                </div>
                                <div class="col-2">
                                      <div class="p-t-30">
                                    <asp:Button ID="Button1" runat="server" Text="Home" CssClass="btn btn--radius btn--green" BackColor="LightBlue" OnClick="ButtonRegister_Click"  ValidationGroup="home"/>
                                </div>
                                </div>
                              
                            </div>
                            <div class="row row-space">
                                <div style="display:inline">
                                </div>
                                <div style="display:inline">
                                    &nbsp;</div>
                            </div>
                            <div class="row row-space">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body>
</html>
