﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mater/MyTemplateMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="mater_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="Server">
    Default
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="row">
        <div class="col-lg-10">
            <h2>Header</h2>
            <pre>To refer a customer to us, you can use the following URLs:
https://www.SmarterASP.NET/index?r=AhmedAlkaff
or
https://www.SmarterASP.NET/index?r=100758893

[Once your customer visit our site through the URL above, 
your referral ID will be recorded into their browser's cookie. 
At anytime when your customer decides to signup, you'll get credited.]

You can also use the follow banners we provide:</pre>
        </div>

        <div class="col-lg-2" >
            <img src="../images/bg-heading-02.jpg" alt="Picture" style="max-height: 300px" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="input-group">
                 <asp:TextBox ID="TextBox1" runat="server" CssClass="text-center" placeholder="First Name"></asp:TextBox>
            </div>
        </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="input-group">
                 <asp:TextBox ID="TextBox2" runat="server" CssClass="text-center" placeholder="Second Name"></asp:TextBox>
            </div>
        </div>
      
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="input-group">
                 <asp:TextBox ID="TextBox3" runat="server" CssClass="text-center" placeholder="Last Name"></asp:TextBox>
            </div>
        </div>

        
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <div class="input-group">
                <asp:Button ID="Button2" runat="server" Text="Button" CssClass="btn-default" />
            </div>
        </div>
        
   
       
    </div>
</asp:Content>

