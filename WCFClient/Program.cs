﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFClient.MVFServiceReference;

namespace WCFClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WCFTesterClient client = new WCFTesterClient("BasicHttpBinding_IWCFTester"))
            {
                Console.WriteLine("From HTTP for HelloWorld {0,30}", client.HelloWorld());
                Console.WriteLine("From HTTP for SayHello {0,30}", client.SayHello("Ahmed"));

            }
            Console.WriteLine();

            using (WCFTesterClient client = new WCFTesterClient("NetTcpBinding_IWCFTester"))
            {
                Console.WriteLine("From NET TCP for HelloWorld {0,30}", client.HelloWorld());
                Console.WriteLine("From NET TCP for SayHello {0,30}", client.SayHello("Ahmed"));
            }

            Console.ReadKey();
        }
    }
}
