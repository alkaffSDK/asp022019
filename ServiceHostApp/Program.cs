﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ServiceHostApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(MyWCFService.WCFTester)))
            {
                host.Open();
                Console.WriteLine("The host stat is {0} @ {1}", host.State , DateTime.Now);
                do
                    Console.WriteLine("Press Esc to stop the host.");
                while (!Console.ReadKey().Key.Equals(ConsoleKey.Escape));

                Console.WriteLine("Thanks ");
                Console.ReadKey();
            }
        }
    }
}
