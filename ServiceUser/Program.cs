﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceUser
{
    class Program
    {
        static void Main(string[] args)
        {
            using (DemoServiceReference.DemoServiceSoapClient client = new DemoServiceReference.DemoServiceSoapClient())
            {
                client.Open();
                Console.WriteLine("Hello world :"+ client.HelloWorld());
                string name = "Ahmed";
                Console.WriteLine("SayHello :"+ client.SayHello(name));
                Console.WriteLine("Add 50, 17 :" + client.Add(50,17));
               
              
            }

            Console.ReadKey();
        }
    }
}
